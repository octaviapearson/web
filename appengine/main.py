# -*- coding: utf-8 -*-
#!/usr/bin/env python


# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import webapp2
import database
from results import Results

BULKMAX=128

def keylength(key):
    s=""
    for i in range(0,len(key)): s+="*"
    return s+" ("+str(len(key))+" символов"+")"

class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers.add_header("Access-Control-Allow-Origin", "*")
        count = self.request.get('n')
        print(count)
        if (count==""): count=1
        count=int(count)
        if(count>BULKMAX):
            self.response.write("Количество предложений для генерации (n) не должно превышать " + str(BULKMAX) + "!")
            return
        sentences=self.getsentences(count)
        result=None
        finalpage=""
        for snt in sentences:
            result=snt[0]
            if(result==Results.NORECORD):
                break
            if(result==Results.OK):
                finalpage+=snt[1]+"\n<br>"
        self.response.headers.add_header("RESULT", result)
        if (result==Results.NORECORD):
            self.response.write("Ошибка с базой данных!")
        if (result==Results.OK):
            self.response.write(finalpage)

    def getsentences(self,count):
        sentences=[]
        for i in range(0,count):
            result=database.getsentence()
            sentences.append(result)
        return sentences

class AddRecord(webapp2.RequestHandler):
    def get(self):
        self.response.headers.add_header("Access-Control-Allow-Origin", "*")
        key = self.request.get('key')
        result=database.addrecord(key)
        self.response.headers.add_header("RESULT",result)
        if (result==Results.FAILED):
            self.response.write("В базе данных уже есть записи!")
            self.response.write("<br>Ключ (key): "+keylength(key))
            return
        if (result==Results.OK):
            self.response.write("Ура! Вы добавили запись!")
            self.response.write("<br>Ключ (key): "+keylength(key))
            return
        self.response.write("Неправильный ключ")
        self.response.write("<br>Ключ (key): "+keylength(key))

class ChangeKey(webapp2.RequestHandler):
    def get(self):
        self.response.headers.add_header("Access-Control-Allow-Origin", "*")
        key = self.request.get('key')
        newkey = self.request.get('newkey')
        result=database.changekey(key,newkey)
        self.response.headers.add_header("RESULT",result)
        if(result==Results.OK):
            self.response.write("Ура! Вы смогли изенить ключ!")
        if(result==Results.WRONGKEY):
            self.response.write('О нет! Вы не смогли изменить ключ! Ваш ключ не правильный!')
        self.response.write("<br>Ключ (key): "+keylength(key)+
                            "<br>Новый ключ (newkey): "+keylength(newkey))

class AddWord(webapp2.RequestHandler):
    def get(self):
        self.response.headers.add_header("Access-Control-Allow-Origin", "*")
        key = self.request.get('key')
        word = self.request.get('word')
        group = self.request.get('group')
        result=database.addword(key,word,group)
        self.response.headers.add_header("RESULT", result)
        if (result==Results.OK):
            self.response.write('Слово '+word.encode('utf-8') + ' добавлено в '+group.encode('utf-8')+''+
            "<br><a href=\"delword?word="+word.encode('utf-8')+"&group="+group.encode('utf-8')+"&key="+key.encode()+"\">Чтобы удалить, нажмите сюда</a>")
        if (result==Results.NORECORD):
            self.response.write('Нет записи! Попробуйте /addrecord?key='+keylength(key))
        if (result==Results.NOGROUP):
            self.response.write('Категории '+group.encode('utf-8')+' не существует!')
        if (result==Results.WRONGKEY):
            self.response.write('Ваш ключ не подходит!')
        self.response.write("<br>Ключ (key): "+keylength(key)+'<br>Слово (word): '+word.encode('utf-8')+'<br>Категория (group): '+group.encode('utf-8'))


class GetJSON(webapp2.RequestHandler):
    def get(self):
        self.response.headers.add_header("Access-Control-Allow-Origin", "*")
        self.response.headers.add_header("Charset", "utf-8")
        self.response.write(database.getjson().encode('utf-8'))

class DeleteWord(webapp2.RedirectHandler):
    def get(self):
        self.response.headers.add_header("Access-Control-Allow-Origin", "*")
        key = self.request.get('key')
        word = self.request.get('word')
        group = self.request.get('group')
        result=database.delword(key,word,group)
        self.response.headers.add_header("RESULT", result)
        if (result==Results.OK):
            self.response.write('Слово '+word.encode('utf-8') + ' удалено из '+group.encode('utf-8')+'')
        if (result==Results.FAILED):
            self.response.write('Слова '+word.encode('utf-8') + ' нет в '+group.encode('utf-8')+'')
        if (result==Results.NORECORD):
            self.response.write('Нет записи! Попробуйте /addrecord?key='+keylength(key))
        if (result==Results.NOGROUP):
            self.response.write('Категории '+group.encode('utf-8')+' не существует!')
        if (result==Results.WRONGKEY):
            self.response.write('Ваш ключ не подходит!')
        self.response.write("<br>Ключ (key): "+keylength(key)+'<br>Слово (word): '+word.encode('utf-8')+'<br>Категория (group): '+group.encode('utf-8'))

app = webapp2.WSGIApplication([
    ('/', MainHandler), ("/getjson",GetJSON) ,("/addrecord", AddRecord), ("/changekey",ChangeKey), ("/addword",AddWord), ("/delword", DeleteWord)
], debug=True)
